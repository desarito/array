import os, requests, threading, datetime, random
from pyvirtualdisplay import Display
from selenium import webdriver
from time import sleep

def send_start(worker_name):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/beacon/', data={'worker': worker_name}, timeout=5)
    except:
        print('err sent_beacon()')

def send_finish(worker_name, th, ah):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/', data={'worker': worker_name, 'th':th, 'ah':ah}, timeout=5)
    except:
        print('err send_finish()')

def main():
    with open('id', 'r') as f:
        worker_name = f.read()
    with open('node', 'r') as f:
        node = f.read()
    send_start(worker_name)
    display = Display(visible=0, size=(1366, 768))
    display.start()
    driver = webdriver.Firefox()
    driver.get(node)
    sleep(60*random.randint(5,20))
    th = 0#driver.find_element_by_id('th').text
    ah = 0#driver.find_element_by_id('ah').text
    driver.close()
    display.stop()
    send_finish(worker_name, th, ah)

if __name__ == '__main__':
    main()
#2018-02-27 09:34:01.188522
#2018-02-27 09:57:01.942055
#2018-02-27 10:20:04.010188
#2018-02-27 10:39:01.656634
#2018-02-27 11:02:01.761418
#2018-02-27 11:17:02.095478
#2018-02-27 11:41:01.578346
#2018-02-27 12:07:01.515625
#2018-02-27 12:27:01.258318
#2018-02-27 12:52:01.248097
#2018-02-27 13:23:01.869631
#2018-02-27 13:46:01.797631
#2018-02-27 14:01:02.133873
#2018-02-27 14:24:02.123308
#2018-02-27 14:49:01.897764
#2018-02-27 15:09:01.995170
#2018-02-27 15:24:02.140931
#2018-02-27 15:45:03.144590
#2018-02-27 16:14:01.748315
#2018-02-27 16:32:01.685678
#2018-02-27 17:32:01.662177
#2018-02-27 18:17:01.201914
#2018-02-27 19:19:01.502035
#2018-02-27 20:34:01.880362